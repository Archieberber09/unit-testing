//PACKAGES AND VARIABLES
const express = require("express");
const app = express();
const PORT = 3000;

//MIDDLEWARE
app.use(express.json());

//Route

app.use("/", (req, res) => {
    res.send({ "data": {} })
})

//Initialize server
app.listen(PORT, () => {
    console.log(`App is running on port ${PORT}`)
})