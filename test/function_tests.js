// TEST SUITE DEMO
// mocha -> unit testing framework
    // describe() -> is not part of javascript, but rather it is a function defined in the library that we used (mocha).
    // --> is a function in the Mocha testing framework. It simply describes the suite of test cases enumerated by the "it" function. --> Also used in the Jasmine Framework.
    // -->describing only the functions in one group or one test suite family

    // IMPORT DEPENDENCY 
const { expect } = require("chai");

// IMPORT FUNCTIONS
const { exchange, exchangeRates } = require("../src/utils");

    describe('test_exchange_usd_to_multiple_currencies', () => {
        // convert usd to php
        it('test_exchange_usd_to_php', () => {
            const phpValue = exchange('usd', 'peso', 1000)
            expect(phpValue).to.equal(50730)
        })
    
        // usd to yen
        it('test_exchange_usd_to_yen', () => {
            const yenValue = exchange('usd', 'yen', 1000)
            expect(yenValue).to.equal(108630)
        })
    
        // usd to yuan
        it("test_exchange_usd_to_yuan", () => {
        const yuanValue = exchange("usd", "yuan", 1000);
        expect(yuanValue).to.equal(7030);
        })
    
        // usd to won
        it("test_exchange_usd_to_won", () => {
            const wonValue = exchange("usd", "won", 1000);
            expect(wonValue).to.equal(1187240)
        })
    })
    
    // ACTIVITY for SUITE DEMO
    describe('test_exchange_peso_to_multiple_currencies', () => {
        it("test_exchange_peso_to_usd", () => {
        const usdValue = exchange("peso", "usd", 1000);
        expect(usdValue).to.equal(20)
        })
    
        it("test_exchange_peso_to_won", () => {
            const wonValue = exchange("peso", "won", 1000);
            expect(wonValue).to.equal(23390)
        })
    
        it("test_exchange_peso_to_yen", () => {
            const yenValue = exchange("peso", "yen", 1000);
            expect(yenValue).to.equal(2140)
        })
    
        it("test_exchange_peso_to_yuan", () => {
            const yuanValue = exchange("peso", "yuan", 1000);
            expect(yuanValue).to.equal(140)
        })
    })


    