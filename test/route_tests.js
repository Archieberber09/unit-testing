// Import Dependencies

const chai = require('chai') //Assertion Library
const {expect} = chai //Style
const chaiHttp = require('chai-http') //Request hadler
chai.use(chaiHttp)

// Server Url
const serverUrl = 'http://localhost:3000'

// Test suite
describe('GET ALL RATES',()=>{
    it('should accept http requests',()=>{
        chai
        .request(serverUrl)
        .get('/rates')
        .end((err,res)=>expect(res).to.not.equal(undefined))
    })

    it('should have a status of 200',()=>{
        chai 
        .request(serverUrl)
        .get('/rates')
        .end((err,res)=>{
            expect(res).to.have.status(200)
            // done()
        })
    })

    it('should return an object w/ a property of rates',()=>{
        chai 
        .request(serverUrl)
        .get('/rates')
        .end((err,res)=>{
            expect(res.body).to.have.property('rates')
            // done()
        })
    })

    it('should have an object w/ a property of rates w/ the length of 5',()=>{
        chai
        .request(serverUrl)
        .get('/rates')
        .end((err,res)=>{
            expect(Object.keys(res.body.rates).length(5))
            // done()
        })
    })

})              

